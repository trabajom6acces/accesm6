package dao;

import java.util.List;

import Entidades.Lot;
import Entidades.Producte;

public interface ILotDao extends IGenericDao<Lot, Integer> {

	void saveOrUpdate(Lot entity);

	Lot get(Integer id);

	List<Lot> list();

	void delete(Integer id);

//	boolean afegirLot(Integer p, Lot l);
	
}
 