package dao;

import java.util.List;
import java.util.Set;

import Entidades.Direccio;
import Entidades.PeticionsProveidor;
import Entidades.Proveidor;

public interface IProveidorDao extends IGenericDao<Proveidor,Integer>{
	
	void saveOrUpdate(Proveidor entity);

	Proveidor get(Integer id);

	List<Proveidor> list();

	void delete(Integer id);

	boolean afegirDireccio(Proveidor p, Direccio d);
	
	boolean afegirPeticionsProveidor(Set<PeticionsProveidor> peticions);
}
 