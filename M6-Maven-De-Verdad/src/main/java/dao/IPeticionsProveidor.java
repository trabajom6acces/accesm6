package dao;

import java.util.List;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import Entidades.Lot;
import Entidades.PeticionsProveidor;
import Entidades.Producte;
import Entidades.Proveidor;

public interface IPeticionsProveidor extends IGenericDao<PeticionsProveidor,Integer>{

	void saveOrUpdate(PeticionsProveidor entity);

	PeticionsProveidor get(Integer id);

	List<PeticionsProveidor> list();

	void delete(Integer id);
	
	boolean afegirProveidor(PeticionsProveidor pp, Proveidor p);
	
	boolean afegirProducte(PeticionsProveidor pp, Producte p);
}
 