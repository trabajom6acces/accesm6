package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import Entidades.PeticionsProveidor;
import Entidades.Producte;
import Entidades.Proveidor;

public class PeticionsProveidorDao extends GenericDao<PeticionsProveidor,Integer> implements IPeticionsProveidor{

	@Override
	public boolean afegirProveidor(PeticionsProveidor pp, Proveidor p) {
		
		Session session = sessionFactory.getCurrentSession();
		
		p.getPeticionsProvedor().add(pp);
		try {
			session.beginTransaction();
			session.saveOrUpdate(p);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public boolean afegirProducte(PeticionsProveidor pp, Producte p) {
		Session session = sessionFactory.getCurrentSession();
		
		p.getPeticionsProveidor().add(pp);
		try {
			session.beginTransaction();
			session.saveOrUpdate(p);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}

}
