package dao;

import java.util.List;
import java.util.Set;

import Entidades.Lot;
import Entidades.Producte;

public interface IProducteDao extends IGenericDao<Producte,Integer>{
	
	void saveOrUpdate(Producte entity);

	Producte get(Integer id);

	List<Producte> list();

	void delete(Integer id);

	boolean afegirLot(Producte p, Lot l);
	boolean afegirLot(Producte p, Lot l, boolean newTransaction);
	
	boolean afegirComposicio(Producte p, Set<Producte> composicio);
	
}
 