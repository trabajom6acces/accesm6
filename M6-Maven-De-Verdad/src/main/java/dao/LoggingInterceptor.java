package dao;

import java.io.Serializable;
import java.util.Date;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import Entidades.*;
import Principal.MainDao;

public class LoggingInterceptor extends EmptyInterceptor {

	private static final long serialVersionUID = 1L;
	// Define a static logger
	// private static Logger logger =
	// LogManager.getLogger(LoggingInterceptor.class);

	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		System.out.println("onSave method is called.");
	
		
		if (entity instanceof Producte) {
			Producte p = (Producte) entity;
			Lot lot = new Lot(100, new Date(), new Date());
			System.out.println(p.toString());

			// metodo que comprueba que no haya menos de 0
			if (p.getStock() < p.getStockMinim()) {
				MainDao.producd.afegirLot(p, lot);
				lot.setProducte(p);
				MainDao.lotd.saveOrUpdate(lot,false);
				System.out.println("*****************estoy aqui");
				
			}

		}
		
		if(entity instanceof Lot)
		{
			Lot lote = (Lot) entity;
			Producte p = lote.getProducte();
			p.setStock(p.getStock()+lote.getQuantitat());
			System.out.println(p.getStock());
			System.out.println(lote.getQuantitat());
			MainDao.producd.saveOrUpdate(p,false);
		}
		
		return super.onSave(entity, id, state, propertyNames, types);

	}
	
	

	@Override
	public boolean onLoad(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		System.out.println("onLoad method is called.");
		System.out.println(id);
		// if (entity instanceof Profesor) {
		// Profesor p = (Profesor) entity;
		// int horas = p.getHorasAsignadas();
		// for (Object o: state) {
		// System.out.println(o);
		// }
		// if(horas<10) {
		// System.out.println("Dale mas curro a este profesor que esta vagote, Alex");
		// }
		// System.out.println(p);
		// String nom = p.getNombre();
		// System.out.println(nom);
		// //if(nom.equals("Gregorio")) {
		// // System.out.println("MICROSOFT ACCEEEEEES!!!!!!!!");
		// //}

		//
		// }
		return super.onLoad(entity, id, state, propertyNames, types);

	}

	@Override
	public String onPrepareStatement(String sql) {
		// logger.info(sql);
		return super.onPrepareStatement(sql);
	}

}