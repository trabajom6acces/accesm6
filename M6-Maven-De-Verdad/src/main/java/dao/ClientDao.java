package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.Direccio;

public class ClientDao extends GenericDao<Client, Integer> implements IClientDao {

	@Override
	public boolean afegirComanda(Client c, Comanda com) {
		Session session = sessionFactory.getCurrentSession();
		c.getComandes().add(com);	
		com.setClient(c);
		try {
			session.beginTransaction();			
			session.saveOrUpdate(c);
			session.saveOrUpdate(com);
			
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public boolean afegirDireccio(Client c, Direccio d) {
		Session session = sessionFactory.getCurrentSession();
		c.setAdreca(d); 
		try {
			session.beginTransaction();
			session.saveOrUpdate(c);
			session.saveOrUpdate(d);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}
}
