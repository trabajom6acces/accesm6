package Entidades;


import java.util.HashSet;
import java.util.Set;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Client")
public class Client implements Serializable {
	
	 
	@Id
	@Column(name = "NIF", length = 9)
	protected String NIF;
	
	@Column(name="nom_client")
	protected String nomClient;
	
	@Column(name="actiu")
	protected boolean actiu;

	//relacio 1 a n amb comanda
	@OneToMany(mappedBy="client")
	Set<Comanda> comandes = new HashSet<>();
	
	//relacio 1 a 1 amb adreça
	@JoinColumn(name="direccio", nullable=true)
	@OneToOne(cascade = CascadeType.PERSIST)
	private Direccio direccio;

	public Client() {
		
	}
	public Client(String nIF, String nomClient, boolean actiu) {
		super();
		NIF = nIF;
		this.nomClient = nomClient;
		this.actiu = actiu;
		
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		//cada clase debe tener un result distinto
		int result = 898878887;
		result = prime * result + NIF.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Client))
			return false;
		Client other = (Client) obj;
		if (NIF != other.NIF)
			return false;
		return true;
	}
	public String getNIF() {
		return NIF;
	}
	public void setNIF(String nIF) {
		NIF = nIF;
	}
	public String getNomClient() {
		return nomClient;
	}
	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}
	public boolean isActiu() {
		return actiu;
	}
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	public Set<Comanda> getComandes() {
		return comandes;
	}
	public void setComandes(Set<Comanda> comandes) {
		this.comandes = comandes;
	}
	public Direccio getAdreca() {
		return direccio;
	}
	public void setAdreca(Direccio adreca) {
		this.direccio = adreca;
	}
	
	
	
}