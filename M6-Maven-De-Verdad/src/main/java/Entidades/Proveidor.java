package Entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Proveidor")
public class Proveidor {

	@Id
	@Column(name = "CIFF", updatable = true, nullable = false)
	private int idProveidor;

	@Column(name = "nom")
	private String nomProveidor;

	@Column(name = "telefon")
	private String telefon;
 
	@Column(name = "personaContacte")
	private String personaContacte;

	@Column(name = "status")
	private boolean actiu;

	// relacio 1 a 1 amb address
	// relacio 1 a n amb peticioproveidor

	// OneToOne, simplemente pones el objeto al que referencias
	@JoinColumn(name = "addresss", nullable = false)
	@OneToOne(cascade = CascadeType.PERSIST)
	private Direccio direccio;

	// oneToMany. ahi decimos de que queremos el set (que tabla) y dentro de esa
	// tabla cual es la columna que contiene
	// la llave foranea, es decir, el curso. Cuidado con las mayusculas, no seas
	// imbecil como yo
	@OneToMany(mappedBy = "proveidor")
	Set<PeticionsProveidor> PeticionsProvedor = new HashSet<PeticionsProveidor>();

	
	
	public Proveidor() {
		
	}
	
	public Proveidor(String nomProvedor, String telefon, String personaCont, boolean actiu) {
		super();
		idProveidor = hashCode();
		this.nomProveidor = nomProvedor;
		this.telefon = telefon;
		this.personaContacte = personaCont;
		this.actiu = actiu;
		
	}

	
	public int getIdProveidor() {
		return idProveidor;
	}

	public void setIdProveidor(int idProveidor) {
		this.idProveidor = idProveidor;
	}

	public String getNomProveidor() {
		return nomProveidor;
	}

	public void setNomProveidor(String nomProveidor) {
		this.nomProveidor = nomProveidor;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getPersonaContacte() {
		return personaContacte;
	}

	public void setPersonaContacte(String personaContacte) {
		this.personaContacte = personaContacte;
	}

	public boolean isActiu() {
		return actiu;
	}

	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	
	public Direccio getDireccio() {
		return direccio;
	}

	public void setDireccio(Direccio direccio) {
		this.direccio = direccio;
	}

	public Set<PeticionsProveidor> getPeticionsProvedor() {
		return PeticionsProvedor;
	}

	public void setPeticionsProvedor(Set<PeticionsProveidor> peticionsProvedor) {
		PeticionsProvedor = peticionsProvedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		// cada clase debe tener un result distinto
		int result = 11112;
		result = prime * result + idProveidor;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Proveidor))
			return false;
		Proveidor other = (Proveidor) obj;
		if (idProveidor != other.idProveidor)
			return false;
		return true;
	}

}
