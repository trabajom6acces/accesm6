package Entidades;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Lot")
public class Lot {

	@Id
	@Column(name = "id", updatable = true, nullable = false)
	private int id;
 
	@Column(name = "quantitat")
	private int quantitat;

	// Temporal tiene varios tipos. Puedes ponerle Date (solo mes/dia), Time (solo
	// hora/minuto), o Timestamp (todo)
	@Temporal(TemporalType.DATE)
	@Column(name = "dataEntrada")
	private Date dataEntrada;

	@Temporal(TemporalType.DATE)
	@Column(name = "dataCaducitat")
	private Date dataCaducitat;

	// relacio n a 1 amb producte
	@ManyToOne
	@JoinColumn(name = "producte")
	private Producte producte;
	

	public Lot() {
		super();
	}

	public Lot(int quantitat, Date dataEntrada, Date dataCaducitat) {
		super();
		this.id =  hashCode();
		this.quantitat = quantitat;
		this.dataEntrada = dataEntrada;
		this.dataCaducitat = dataCaducitat;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataCaducitat() {
		return dataCaducitat;
	}

	public void setDataCaducitat(Date dataCaducitat) {
		this.dataCaducitat = dataCaducitat;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	// relacion manytoone. Aqui declaramos la llave foranea que basicamente es un
	// objeto del tipo referenciado
	// en nuestro caso, de clase curso. Referenciamos su llave primaria pero ponemos
	// el objeto entero. El nombre del objeto
	// equivale al MappedBy de la clase Curso

	@Override
	public int hashCode() {
		final int prime = 31;
		// cada clase debe tener un result distinto
		int result = 22221;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Lot))
			return false;
		Lot other = (Lot) obj;
		if (id != other.id)
			return false;
		return true;
	}

}