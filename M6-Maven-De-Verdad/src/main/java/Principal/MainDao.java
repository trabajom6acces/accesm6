package Principal;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.resource.beans.spi.ProvidedInstanceManagedBeanImpl;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.ComandaEstat;
import Entidades.Direccio;
import Entidades.Lot;
import Entidades.PeticionsProveidor;
import Entidades.Producte;
import Entidades.Proveidor;
import Entidades.Tipus;
import Entidades.UnitatMesura;
import dao.ClientDao;
import dao.ComandaDao;
import dao.DireccioDao;
import dao.LotDao;
import dao.PeticionsProveidorDao;
import dao.ProducteDao;
import dao.ProveidorDao;
import dao.SaveUpdateEventListenerImp;


public class MainDao {
	
	public static ProducteDao producd = new ProducteDao();	
	public static LotDao lotd = new LotDao();		
	
	public static void main(String[] args) { 
		
		SaveUpdateEventListenerImp suel = new SaveUpdateEventListenerImp();
		
		ClientDao clientd = new ClientDao();
		//DireccioDao dired = new DireccioDao();
		ComandaDao comd = new ComandaDao();
		
//		ProducteDao producd = new ProducteDao();	
		ProveidorDao provd = new ProveidorDao();
		PeticionsProveidorDao petid = new PeticionsProveidorDao();
		Date fechaHoy = new Date();
		Date fecha = new Date();//TODO CAMBIAR A OTRA FECHA
		
		fecha.setMonth(5);	
		Direccio d1 = new Direccio(4, "carrer algo", "08208", "holanda", 789789, 87987);
		Comanda com = new Comanda(fechaHoy, fecha, ComandaEstat.PENDENT);
		Client c1 = new Client("12345678D", "Kernel Albareda", true);
		Lot lot1 = new Lot(6,new Date(),new Date());	
		Producte  p1 = new Producte("nombreprod", 4 , 5, UnitatMesura.GRAMS, Tipus.INGREDIENT, 1.2);
		Producte  p2 = new Producte("nombre2", 2 , 0, UnitatMesura.GRAMS, Tipus.INGREDIENT, 1.2);
		Producte  p3 = new Producte("nombre3", 2 , 0, UnitatMesura.GRAMS, Tipus.VENDIBLE, 1.2);
		Proveidor prov1 = new Proveidor("provpepito","5454545454","Persona de contacte", true);
		PeticionsProveidor peti1 = new PeticionsProveidor (1,1,fechaHoy,fechaHoy, ComandaEstat.PENDENT);
			
		producd.save(p1);
		producd.save(p2);
//		producd.afegirComposicio(p1,p1.getComposicio());
////		producd.afegirLot(p1, lot1);
//		clientd.afegirDireccio(c1, d1);
//		com.setClient(c1);
//		comd.save(com);
//		p3.getComposicio().add(p1);
//		producd.save(p3);
//		peti1.setProducte(p1);
//		prov1.setDireccio(d1);
//		peti1.setProveidor(prov1);
//		petid.save(peti1);
//		prov1.getPeticionsProvedor().add(peti1);
//		provd.save(prov1);		
//		
//		p1.setStock(2);
//		producd.save(p1);
//		producd.saveOrUpdate(p1);
		
		}
}