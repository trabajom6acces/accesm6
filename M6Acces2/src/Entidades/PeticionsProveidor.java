package Entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PeticionsProveidor")
public class PeticionsProveidor {

	@Id
	@Column(name = "id", updatable = true, nullable = false)
	private int id;

	@Column(name = "tipus_Peticio")
	private int tipusPeticio;

	@Column(name = "quant_producte")
	private int quantproducte;

	// Temporal tiene varios tipos. Puedes ponerle Date (solo mes/dia), Time (solo
	// hora/minuto), o Timestamp (todo)

	@Temporal(TemporalType.DATE)
	@Column(name = "data_Ordre")
	private Date dataOrdre;

	@Temporal(TemporalType.DATE)
	@Column(name = "data_Recepcio")
	private Date dataRecepcio;

	// ordinal, pone 0,1,2 a los enums. String pone el nombre mismo como un varchar
	@Enumerated(EnumType.STRING)
	@Column(name = "PeticionsProveidor")
	ComandaEstat estat;

	// relacio n a 1 amb proveidor
	@ManyToOne
	@JoinColumn(name = "proveidor")
	private Proveidor proveidor;

	// relacio n a 1 amb producte
	@ManyToOne
	@JoinColumn(name = "producte")
	private Producte producte;

	public PeticionsProveidor() {
		super();
	}

	public PeticionsProveidor( int tipus_peticio, int quant_producte, Date data_Ordre, Date data_Recepcio,
			ComandaEstat estat) {
		super();
		this.id =  hashCode();
		this.tipusPeticio = tipus_peticio;
		this.quantproducte = quant_producte;
		this.dataOrdre = data_Ordre;
		this.dataRecepcio = data_Recepcio;
		this.estat = estat;
	}
 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantproducte() {
		return quantproducte;
	}

	public void setQuantproducte(int quantproducte) {
		this.quantproducte = quantproducte;
	}

	public int getTipusPeticio() {
		return tipusPeticio;
	}

	public void setTipusPeticio(int tipusPeticio) {
		this.tipusPeticio = tipusPeticio;
	}

	public Date getDataOrdre() {
		return dataOrdre;
	}

	public void setDataOrdre(Date dataOrdre) {
		this.dataOrdre = dataOrdre;
	}

	public Date getDataRecepcio() {
		return dataRecepcio;
	}

	public void setDataRecepcio(Date dataRecepcio) {
		this.dataRecepcio = dataRecepcio;
	}

	public ComandaEstat getEstat() {
		return estat;
	}

	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		// cada clase debe tener un result distinto
		int result = 33332;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PeticionsProveidor))
			return false;
		PeticionsProveidor other = (PeticionsProveidor) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
