package Entidades;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Producte")
public class Producte{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_producte")
	protected int codiProducte;
	
	@Column(name="nom_producte")
	protected String nomProducte;
	
	@Column(name="stock")
	protected int stock;
	
	@Column(name="stockMinim")
	protected int stockMinim;
	
	@Enumerated(EnumType.STRING)
	@Column(name="unitat")
	protected UnitatMesura unitat;
	
	@Enumerated(EnumType.STRING)
	@Column(name="tipus")
	protected Tipus tipus;
	
	@Column(name="preu_venda")
	protected double preuVenda;
	
	//relacio 1 a n amb lot
	@OneToMany(mappedBy="Lot")
	private Set<Lot> lotes = new HashSet<>();
	
	//relacio n a n amb ell mateix
	@JoinTable(name="ProducteProducte", joinColumns={@JoinColumn(name="id_producte")}, inverseJoinColumns={@JoinColumn(name="id_producte")})
	@ManyToMany(cascade=CascadeType.REFRESH)
	private Set<Producte> composicio = new HashSet<>();
	
	//relacio 1 a n amb peticionsproveidor
	@OneToMany(mappedBy="PeticioProveidor")
	private Set<PeticionsProveidor> peticionsProveidor = new HashSet<>();
	
	//relacio n a n amb comanda
	@JoinTable(name="ProducteComanda", joinColumns= {@JoinColumn(name="id_Producte")}, inverseJoinColumns= {@JoinColumn(name="id_comanda")})
	@ManyToMany(cascade=CascadeType.REFRESH)
	private Set<Producte> productecomanda = new HashSet<>();
	
	public Producte() {
	}

	public Producte(String nomProducte, int stock, int stockMinim, UnitatMesura unitat, Tipus tipus,
			double preuVenda, Set<Lot> lotes, Set<Producte> composicio, Set<PeticionsProveidor> peticionsProveidor,
			Set<Producte> productecomanda) {
		super();
		this.codiProducte = hashCode();
		this.nomProducte = nomProducte;
		this.stock = stock;
		this.stockMinim = stockMinim;
		this.unitat = unitat;
		this.tipus = tipus;
		this.preuVenda = preuVenda;
		this.lotes = lotes;
		this.composicio = composicio;
		this.peticionsProveidor = peticionsProveidor;
		this.productecomanda = productecomanda;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		//cada clase debe tener un result distinto
		int result = 77998987;
		result = prime * result + codiProducte;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Producte))
			return false;
		Producte other = (Producte) obj;
		if (codiProducte != other.codiProducte)
			return false;
		return true;
	}

	public int getCodiProducte() {
		return codiProducte;
	}

	public void setCodiProducte(int codiProducte) {
		this.codiProducte = codiProducte;
	}

	public String getNomProducte() {
		return nomProducte;
	}

	public void setNomProducte(String nomProducte) {
		this.nomProducte = nomProducte;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getStockMinim() {
		return stockMinim;
	}

	public void setStockMinim(int stockMinim) {
		this.stockMinim = stockMinim;
	}

	public UnitatMesura getUnitat() {
		return unitat;
	}

	public void setUnitat(UnitatMesura unitat) {
		this.unitat = unitat;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

	public double getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public Set<Lot> getLotes() {
		return lotes;
	}
 
	public void setLotes(Set<Lot> lotes) {
		this.lotes = lotes;
	}

	public Set<Producte> getComposicio() {
		return composicio;
	}

	public void setComposicio(Set<Producte> composicio) {
		this.composicio = composicio;
	}

	public Set<PeticionsProveidor> getPeticionsProveidor() {
		return peticionsProveidor;
	}

	public void setPeticionsProveidor(Set<PeticionsProveidor> peticionsProveidor) {
		this.peticionsProveidor = peticionsProveidor;
	}

	public Set<Producte> getProductecomanda() {
		return productecomanda;
	}

	public void setProductecomanda(Set<Producte> productecomanda) {
		this.productecomanda = productecomanda;
	}
	
	
}