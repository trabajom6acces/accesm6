package Entidades;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Direccio")
public class Direccio {

	@Id
	@Column(name = "id", updatable = true, nullable = false)
	int id;

	@Column(name = "bloc")
	private int bloc;

	@Column(name = "carrer")
	private String carrer;

	@Column(name = "codi_postal")
	private String codiPostal;

	@Column(name = "pais")
	private String pais;

	@Column(name = "latitud")
	private double latitud;

	@Column(name = "longitud")
	private double longitud;

	// relacio 1 a 1 amb client
	// OneToOne, simplemente pones el objeto al que referencias
	@JoinColumn(name = "client", nullable = false)
	@OneToOne(cascade = CascadeType.PERSIST)
	private Client client;

	// relacio 1 a 1 amb partner
	// OneToOne, simplemente pones el objeto al que referencias
	@JoinColumn(name = "partner", nullable = false)
	@OneToOne(cascade = CascadeType.PERSIST)
	private Proveidor proveidor;

	public Direccio() {
		super();
	}

	public Direccio(int bloc, String carrer, String codiPostal, String pais, double latitud, double longitud) {
		super();
		this.id =  hashCode();
		this.bloc = bloc;
		this.pais = pais;
		this.codiPostal = codiPostal;
		this.carrer = carrer;
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBloc() {
		return bloc;
	} 

	public void setBloc(int bloc) {
		this.bloc = bloc;
	}

	public String getCarrer() {
		return carrer;
	}

	public void setCarrer(String carrer) {
		this.carrer = carrer;
	}

	public String getCodiPostal() {
		return codiPostal;
	}

	public void setCodiPostal(String codiPostal) {
		this.codiPostal = codiPostal;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Proveidor getProveidor() {
		return proveidor;
	}

	public void setProveidor(Proveidor proveidor) {
		this.proveidor = proveidor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		// cada clase debe tener un result distinto
		int result = 13212;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Direccio))
			return false;
		Direccio other = (Direccio) obj;
		if (id != other.id)
			return false;
		return true;
	}

}