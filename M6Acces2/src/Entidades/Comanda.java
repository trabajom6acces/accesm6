package Entidades;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
 
@Entity
@Table(name = "Comanda")
public class Comanda {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_comanda")
	protected int idComanda;
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_comanda")
	protected Date dataComanda;
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_lliurament")
	protected Date dataLliurament;
	
	@Enumerated(EnumType.STRING)
	@Column(name="esta_comanda")
	protected ComandaEstat estat;	//PENDENT - PREPARAT - TRANSPORT - LLIURAT
	
	//relacio n a n amb producte

	@JoinTable(name="ProducteComanda", joinColumns={@JoinColumn(name="id_producte")}, inverseJoinColumns={@JoinColumn(name="id_comanda")})
	@ManyToMany(cascade=CascadeType.REFRESH)
	private Set<Producte> comandes = new HashSet<>();
		
	//relacio n a 1 amb client
	@JoinColumn(name="Comprador", nullable=false)
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Client comprador;
	
	public Comanda() {	
		super();
	}
	
	public Comanda(Date dataComanda, Date dataLliurament, ComandaEstat estat, Set<Producte> comandes,
			Client comprador) {
		super();
		this.idComanda =  hashCode();
		this.dataComanda = dataComanda;
		this.dataLliurament = dataLliurament;
		this.estat = estat;
		this.comandes = comandes;
		this.comprador = comprador;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		//cada clase debe tener un result distinto
		int result = 8798887;
		result = prime * result + idComanda;
		return result;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Comanda))
			return false;
		Comanda other = (Comanda) obj;
		if (idComanda != other.idComanda)
			return false;
		return true;
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public Date getDataComanda() {
		return dataComanda;
	}

	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public ComandaEstat getEstat() {
		return estat;
	}

	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}

	public Set<Producte> getComandes() {
		return comandes;
	}

	public void setComandes(Set<Producte> comandes) {
		this.comandes = comandes;
	}

	public Client getComprador() {
		return comprador;
	}

	public void setComprador(Client comprador) {
		this.comprador = comprador;
	}
	
	
}