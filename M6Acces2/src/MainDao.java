
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.ComandaEstat;
import Entidades.Direccio;
import Entidades.Lot;
import Entidades.PeticionsProveidor;
import Entidades.Producte;
import Entidades.Tipus;
import Entidades.UnitatMesura;
import dao.ClientDao;
import dao.ComandaDao;
import dao.DireccioDao;
import dao.LotDao;
import dao.ProducteDao;


public class MainDao {
	
	public static void main(String[] args) { 
		
		ClientDao clientd = new ClientDao();
		DireccioDao dired = new DireccioDao();
		ComandaDao comd = new ComandaDao();
		LotDao lotd = new LotDao();		
		ProducteDao producd = new ProducteDao();		
		Date fechaHoy = new Date();
		Date fecha = new Date();//TODO CAMBIAR A OTRA FECHA
		
		fecha.setMonth(5);		
		
		Client c1 = new Client("76968969D", "Kernel Albareda", true);
		Lot lot1 = new Lot(1,fechaHoy,fecha);
		Comanda com = new Comanda(fechaHoy, fecha, ComandaEstat.PENDENT, new HashSet<Producte>(),c1);		
		Direccio d1 = new Direccio(4, "carrer algo", "08208", "holanda", 789789, 87987);
		Producte  p1 = new Producte("nombreprod", 2 , 0, UnitatMesura.GRAMS, Tipus.INGREDIENT, 1.2,
				new HashSet<Lot>(), new HashSet<Producte>(), new HashSet<PeticionsProveidor>(),
				new HashSet<Producte>());
		Producte  p2 = new Producte("nombre2", 2 , 0, UnitatMesura.GRAMS, Tipus.INGREDIENT, 1.2,
				new HashSet<Lot>(), new HashSet<Producte>(), new HashSet<PeticionsProveidor>(),
				new HashSet<Producte>());
		
		clientd.afegirDireccio(c1, d1);
		clientd.afegirComanda(c1, com);
		clientd.saveOrUpdate(c1);
		
		dired.afegirClient(d1, c1);
		dired.saveOrUpdate(d1);
		
		lotd.afegirLot(p1, lot1);
		lotd.saveOrUpdate(lot1);
		
		comd.afegirProducte(com, p1);
		comd.saveOrUpdate(com);
		
		producd.saveOrUpdate(p1);
		producd.saveOrUpdate(p2);
		
	}

}
