package dao;

import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import Entidades.Lot;
import Entidades.Producte;

public class ProducteDao extends GenericDao<Producte,Integer> implements IProducteDao{

	@Override
	public boolean afegirLot(Producte p, Lot l) {
		Session session = sessionFactory.getCurrentSession();
		p.getLotes().add(l);
		l.setProducte(p);
		try {
			session.beginTransaction();
			session.saveOrUpdate(p);
			session.saveOrUpdate(l);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public boolean afegirComposicio(Producte p, Set<Producte> composicio) {
		Session session = sessionFactory.getCurrentSession();
		p.setComposicio(composicio);
		try {
			session.beginTransaction();
			session.saveOrUpdate(p);

			session.getTransaction().commit();
			return true;
		}catch(HibernateException e) {
			e.printStackTrace();
			if(session!=null&&session.getTransaction()!=null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}

} 
