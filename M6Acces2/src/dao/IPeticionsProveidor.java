package dao;

import java.util.List;

import Entidades.PeticionsProveidor;

public interface IPeticionsProveidor extends IGenericDao<PeticionsProveidor,Integer>{

	void saveOrUpdate(PeticionsProveidor entity);

	PeticionsProveidor get(Integer id);

	List<PeticionsProveidor> list();

	void delete(Integer id);
	
	
}
 