package dao;

import java.util.List;

import Entidades.Client;
import Entidades.Direccio;

public interface IDireccioDao extends IGenericDao<Direccio, Integer> {

	void saveOrUpdate(Direccio entity);

	Direccio get(Integer id);

	List<Direccio> list();

	void delete(Integer id);

	boolean afegirClient(Direccio d, Client c);

	boolean afegirPartner(Direccio d, Client c);
}
 