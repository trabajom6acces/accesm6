package dao;

import java.util.List;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.Direccio;

public interface IClientDao extends IGenericDao<Client,Integer>{

	void saveOrUpdate(Client entity);
	
	Client get(Integer id);
	
	List<Client> list();
	
	void delete(Integer id);
	
	boolean afegirComanda(Client cl,Comanda com);
	
	boolean afegirDireccio(Client c, Direccio d);
}
 