package dao;

import java.util.List;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.Producte;

public interface IComandaDao extends IGenericDao<Comanda, Integer> {

	void saveOrUpdate(Comanda entity);

	Comanda get(Integer id);

	List<Comanda> list();

	void delete(Integer id);

	boolean afegirProducte(Comanda c, Producte p);

	boolean afegirComprador(Comanda com, Client cl);
	
}
 