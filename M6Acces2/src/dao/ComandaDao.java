package dao;

import org.hibernate.Session;
import org.hibernate.HibernateException;

import Entidades.Client;
import Entidades.Comanda;
import Entidades.Producte;

public class ComandaDao extends GenericDao<Comanda, Integer> implements IComandaDao {

	@Override
	public boolean afegirComprador(Comanda com, Client cl) {
		Session session = sessionFactory.getCurrentSession();
		cl.getComandes().add(com);
		com.setComprador(cl);
		try { 
			session.beginTransaction();
			session.saveOrUpdate(cl);
			session.saveOrUpdate(com);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}

	@Override
	public boolean afegirProducte(Comanda c, Producte p) {
		Session session = sessionFactory.getCurrentSession();
		c.getComandes().add(p);
		try {
			session.beginTransaction();
			session.saveOrUpdate(c);
			session.saveOrUpdate(p);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}

}
