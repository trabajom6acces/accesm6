package dao;

import org.hibernate.Session;
import org.hibernate.HibernateException;

import Entidades.Client;
import Entidades.Direccio;

public class DireccioDao extends GenericDao<Direccio, Integer> implements IDireccioDao {

	@Override
	public boolean afegirClient(Direccio d, Client c) {
		Session session = sessionFactory.getCurrentSession();
		d.setClient(c);
		c.setAdreca(d);
		try {
			session.beginTransaction();
			session.saveOrUpdate(d);
			session.saveOrUpdate(c);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	} 

	@Override
	public boolean afegirPartner(Direccio d, Client c) {
		Session session = sessionFactory.getCurrentSession();
		d.setClient(c);
		c.setAdreca(d);
		try {
			session.beginTransaction();
			session.saveOrUpdate(d);
			session.saveOrUpdate(c);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

}
