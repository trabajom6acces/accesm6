package dao;

import org.hibernate.Session;
import org.hibernate.HibernateException;

import Entidades.Lot;
import Entidades.Producte;

public class LotDao extends GenericDao<Lot,Integer>implements ILotDao{

	@Override
	public boolean afegirLot(Producte p, Lot l) {
		Session session = sessionFactory.getCurrentSession();
		p.getLotes().add(l);
		l.setProducte(p);
		try {
			session.beginTransaction();
			session.saveOrUpdate(p);
			session.saveOrUpdate(l);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

} 
