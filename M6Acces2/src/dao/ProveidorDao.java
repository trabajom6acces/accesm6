package dao;

import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import Entidades.Client;
import Entidades.Direccio;
import Entidades.PeticionsProveidor;
import Entidades.Proveidor;

public class ProveidorDao extends GenericDao<Proveidor,Integer> implements IProveidorDao{

	

	@Override
	public boolean afegirPeticionsProveidor(Set<PeticionsProveidor> peticions) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean afegirDireccio(Proveidor p, Direccio d) {
		Session session = sessionFactory.getCurrentSession();
		p.setDireccio(d);
		try { 
			session.beginTransaction();
			session.saveOrUpdate(p);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			return false;
		}
	}
}
